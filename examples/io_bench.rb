# -*- encoding: binary -*-
$stderr.sync = $stdout.sync = true
bs = ENV['bs'] ? ENV['bs'].to_i : 100
count = ENV['count'] ? ENV['count'].to_i : 100000
method = ENV['method'] ? ENV['method'].to_sym : :write_utf8
force_utf8 = ENV['force_utf8']
force_utf8 = force_utf8.to_s !~ /\A(false|no|off)\z/ && force_utf8.to_i != 0
puts "ENV: count=#{count} bs=#{bs} method=#{method} force_utf8=#{force_utf8}"
puts "Set corresponding environment variables to overriden"

require 'benchmark'
require 'sunshowers'

r, w = IO.pipe
r.binmode
w.binmode
slice = ' ' * bs
slice.force_encoding(Encoding::UTF_8) if force_utf8 && defined?(Encoding)
puts Benchmark::Tms::CAPTION

reader = fork do
  r = Sunshowers::IO.new(r)
  w.close
  b = Benchmark.measure { r.each { |_| } }
  $stdout.syswrite("r: #{b}")
  exit 0
end

r.close
w = Sunshowers::IO.new(w)

b = Benchmark.measure do
  case method
  when :write
    count.times { |i| w.write(slice) }
  when :write_utf8
    count.times { |i| w.write_utf8(slice) }
  when :write_binary
    count.times { |i| w.write_binary(slice) }
  else
    abort "method=#{method} must be (write|write_utf8|write_binary)"
  end
end
w.to_io.close
_, status = Process.waitpid2(reader)
$stdout.syswrite("w: #{b}#{status.inspect}\n")
