#\-E deployment
#
# This is designed to run against:
#   http://pywebsocket.googlecode.com/svn/trunk/src/example/echo_client@139
#
require 'sunshowers'
use Rack::ContentLength
use Rack::ContentType
run lambda { |env|
  req = Sunshowers::Request.new(env)
  if req.ws?
    req.ws_handshake!
    ws_io = req.ws_io
    ws_io.each do |record|
      ws_io.write(record)
      break if record == "Goodbye"
    end
    req.ws_quit! # Rainbows! should handle this quietly
  end
  [404, {}, []]
}
