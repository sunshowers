#!/usr/bin/ruby
# this is designed to work with the echo_wsh.py handler for pywebsocket
# -*- encoding: binary -*-
require 'socket'
require 'sunshowers'

host_with_port = ARGV.shift or abort "Usage: #$0 HOST:PORT"
host, port = host_with_port.split(/:/)
port ||= 80

write_method = ARGV.shift || :write_utf8
write_method = write_method.to_sym

req =
  "GET /echo HTTP/1.1\r\n" \
  "Upgrade: WebSocket\r\n" \
  "Connection: Upgrade\r\n" \
  "Host: #{host_with_port}\r\n" \
  "Origin: http://#{host_with_port}/\r\n" \
  "\r\n"

# yes we cut corners here
sock = TCPSocket.new(host, port.to_i)
sock.syswrite(req)
buf = ""
begin
  buf << sock.readpartial(16384)
end while buf !~ /\r\n\r\n/
p [ :handshake_header, buf ]

sock = Sunshowers::IO.new(sock)
sock.write_utf8 "HELLO"
p sock.gets

puts "enter whatever you want, Ctrl-D to exit write_method=#{write_method}"
begin
  while line = STDIN.gets
    sock.__send__ write_method, line
    p sock.gets
  end
rescue EOFError
ensure
  sock.write_utf8 "Goodbye"
  p sock.gets
end
