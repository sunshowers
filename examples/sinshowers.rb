# Example of Sunshowers + Sinatra 0.9.4, for use with echo_client.{rb,py}

require 'sinatra/base'
require 'sunshowers'

# There's probably a better/cleaner way to do this, but we're not
# very familiar with Sinatra...
# Let us know of a better way at sunshowers@librelist.com
class Sinatra::Request < Rack::Request
  include Sunshowers::WebSocket
end

class Sinshowers < Sinatra::Base

  get "/echo" do
    if request.ws?
      request.ws_handshake!
      ws_io = request.ws_io
      ws_io.each do |record|
        ws_io.write_utf8(record)
        break if record == "Goodbye"
      end
      request.ws_quit!
    end
    "You're not using Web Sockets"
  end

end
