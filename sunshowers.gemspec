# -*- encoding: binary -*-

ENV["VERSION"] or abort "VERSION= must be specified"
manifest = File.readlines('.manifest').map! { |x| x.chomp! }

# don't bother with tests that fork, not worth our time to get working
# with `gem check -t` ... (of course we care for them when testing with
# GNU make when they can run in parallel)
test_files = manifest.grep(%r{\Atest/test_.*\.rb\z}).map do |f|
  File.readlines(f).grep(/\bfork\b/).empty? ? f : nil
end.compact

Gem::Specification.new do |s|
  s.name = %q{sunshowers}
  s.version = ENV["VERSION"]

  s.authors = ["Sunshowers hackers"]
  s.date = Time.now.utc.strftime('%Y-%m-%d')
  s.description = File.read("README").split(/\n\n/)[1]
  s.email = %q{sunshowers@librelist.com}
  s.executables = %w()

  s.extra_rdoc_files = File.readlines('.document').map! do |x|
    x.chomp!
    if File.directory?(x)
      manifest.grep(%r{\A#{x}/})
    elsif File.file?(x)
      x
    else
      nil
    end
  end.flatten.compact

  s.files = manifest
  s.homepage = %q{http://rainbows.rubyforge.org/sunshowers/}
  s.summary = %q{Web Sockets for Ruby, Rack+Rainbows!}
  s.rdoc_options = [ "-Na", "-t", "Sunshowers - #{s.summary}" ]
  s.require_paths = %w(lib)
  s.rubyforge_project = %q{rainbows}

  s.test_files = test_files

  # rainbows has a boatload of dependencies
  # required:
  #   unicorn + rack
  # optional:
  #   revactor + rev + iobuffer
  # # the following do not work directly with Sunshowers, yet
  #   rev + iobuffer
  #   eventmachine
  #   espace-neverblock + eventmachine
  #   async_sinatra + sinatra + eventmachine
  # recommended:
  #   Ruby 1.9 + FiberSpawn concurrency model with Rainbows!
  # s.add_dependency(%q<rainbows>, ["~> 0.9.0"])

  # s.licenses = %w(GPLv2 Ruby) # accessor not compatible with older RubyGems
end
