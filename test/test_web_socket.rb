# -*- encoding: binary -*-
require 'rack'
require 'test/unit'
require 'sunshowers'

class TestWebSocket < Test::Unit::TestCase
  include Sunshowers::WebSocket

  def setup
    @env = { "rack.errors" => $stderr }
  end

  def test_ws_protocol
    assert_nil ws_protocol
    @env["HTTP_WEBSOCKET_PROTOCOL"] = "foo"
    assert_equal "foo", ws_protocol
    @env["HTTP_WEBSOCKET_PROTOCOL"] = "foo\x00"
    assert_raises(Sunshowers::HandshakeError) { ws_protocol }
  end

  def test_resource_name_request_uri
    @env["REQUEST_URI"] = "/foo/bar?hello=world"
    assert_equal "/foo/bar?hello=world", ws_resource_name

    # we depend on fullpath if REQUEST_URI is unset
    assert Rack::Request.new(@env).respond_to?(:fullpath)
  end

end
