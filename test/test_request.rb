# -*- encoding: binary -*-
require 'rack'
require 'test/unit'
require 'stringio'
require 'sunshowers'

class TestWebSocket < Test::Unit::TestCase

  def setup
    @env = {
      "rack.errors" => $stderr,
      "HTTP_CONNECTION" => "Upgrade",
      "HTTP_UPGRADE" => "WebSocket",
      "HTTP_ORIGIN" => "nowhere",
      "HTTP_HOST" => "example.com",
      "SERVER_PORT" => "80",
      "PATH_INFO" => "/",
      "QUERY_STRING" => "",
      "rack.url_scheme" => "http",
      "hack.io" => StringIO.new("")
    }
  end

  def test_ws
    req = Sunshowers::Request.new(@env)
    assert req.ws?
  end

  def test_ws_false
    @env.delete "HTTP_UPGRADE"
    req = Sunshowers::Request.new(@env)
    assert ! req.ws?
  end

  def test_resource_name_fallback
    @env["PATH_INFO"] = "/foo/bar"
    @env["QUERY_STRING"] = "hello=world"
    req = Sunshowers::Request.new(@env)
    assert_equal "/foo/bar?hello=world", req.ws_resource_name
  end

  def test_handshake_invalid
    @env.delete "HTTP_UPGRADE"
    req = Sunshowers::Request.new(@env)
    assert_raises(Sunshowers::HandshakeError) { req.ws_handshake! }
  end

  def test_handshake
    req = Sunshowers::Request.new(@env)
    req.ws_handshake!
    expect = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" \
             "Upgrade: WebSocket\r\n" \
             "Connection: Upgrade\r\n" \
             "WebSocket-Origin: nowhere\r\n" \
             "WebSocket-Location: ws://example.com/\r\n\r\n"
    assert_equal expect, @env["hack.io"].string
  end

  def test_handshake_headers
    req = Sunshowers::Request.new(@env)
    req.ws_handshake!([ %w(Set-Cookie foo=bar) ])
    expect = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" \
             "Upgrade: WebSocket\r\n" \
             "Connection: Upgrade\r\n" \
             "WebSocket-Origin: nowhere\r\n" \
             "WebSocket-Location: ws://example.com/\r\n" \
             "Set-Cookie: foo=bar\r\n" \
             "\r\n"
    assert_equal expect, @env["hack.io"].string
  end

  def test_handshake_multi_value_headers
    req = Sunshowers::Request.new(@env)
    req.ws_handshake!([ [ "Set-Cookie", "foo=bar\nhello=world" ] ])
    expect = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" \
             "Upgrade: WebSocket\r\n" \
             "Connection: Upgrade\r\n" \
             "WebSocket-Origin: nowhere\r\n" \
             "WebSocket-Location: ws://example.com/\r\n" \
             "Set-Cookie: foo=bar\r\n" \
             "Set-Cookie: hello=world\r\n" \
             "\r\n"
    assert_equal expect, @env["hack.io"].string
  end

  def test_handshake_multiple_headers
    req = Sunshowers::Request.new(@env)
    req.ws_handshake!([ %w(a b), %w(c d) ])
    expect = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" \
             "Upgrade: WebSocket\r\n" \
             "Connection: Upgrade\r\n" \
             "WebSocket-Origin: nowhere\r\n" \
             "WebSocket-Location: ws://example.com/\r\n" \
             "a: b\r\n" \
             "c: d\r\n" \
             "\r\n"
    assert_equal expect, @env["hack.io"].string
  end

  def test_location_alt_port
    @env["SERVER_PORT"] = "666"
    req = Sunshowers::Request.new(@env)
    assert_equal "ws://example.com:666/", req.ws_location
  end

  def test_location_wss_alt_port
    @env["rack.url_scheme"] = "https"
    req = Sunshowers::Request.new(@env)
    assert_equal "wss://example.com:80/", req.ws_location
  end

  def test_ws_scheme
    assert_equal "ws", Sunshowers::Request.new(@env).ws_scheme
  end

  def test_wss_scheme
    @env["rack.url_scheme"] = "https"
    assert_equal "wss", Sunshowers::Request.new(@env).ws_scheme
  end

  def test_location_wss_port
    @env["rack.url_scheme"] = "https"
    @env["SERVER_PORT"] = "443"
    req = Sunshowers::Request.new(@env)
    assert_equal "wss://example.com/", req.ws_location
  end

  def test_quit
    req = Sunshowers::Request.new(@env)
    exc = nil
    begin
      req.ws_quit!
    rescue => exc
    end
    assert exc.kind_of?(EOFError)
    assert exc.instance_of?(Sunshowers::WebSocket::Quit)
  end

end
