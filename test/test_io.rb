# -*- encoding: binary -*-
require 'test/unit'
require 'stringio'
require 'sunshowers'

# every time something is mocked, trust levels in tests cases dimish
# This is not needed under Ruby 1.8
class MyStringIO < StringIO
  unless instance_methods.include?(:readpartial)
    warn "W: defining readpartial for MyStringIO RUBY_VERSION=#{RUBY_VERSION}"

    def readpartial(length, buf = "")
      seek(tell)
      rv = read(length)
      rv.nil? and raise EOFError
      rv
    end
  end
end

class TestIO < Test::Unit::TestCase

  def setup
    @raw = MyStringIO.new("")
    @io = Sunshowers::IO.new(@raw) # binary
    if defined?(Encoding::Binary)
      assert(Encoding::Binary, @io.to_io.encoding)
    end
  end

  def test_gets
    @raw.string << "\x00FOO\xff"
    rv = @io.gets
    assert_equal "FOO", rv
    assert_equal Encoding::UTF_8, rv.encoding if rv.respond_to?(:encoding)
  end

  def test_gets_bad_utf8
    @raw.string << "\x00\xe0\xff"
    assert_raises(Sunshowers::ProtocolError) { @io.gets }
  end

  def test_gets_bad_alignment
    @raw.string << "\x00FOO"
    assert_nil @io.gets
    @raw.string << "\xff"
    rv = @io.gets
    assert_equal "FOO", rv
    assert_equal Encoding::UTF_8, rv.encoding if rv.respond_to?(:encoding)
  end

  def test_write_utf8
    str = "hello world"
    @io.write_utf8(str)
    wr = @raw.string
    assert_equal "\0#{str}\xff", wr
    assert_equal Encoding::BINARY, wr.encoding if wr.respond_to?(:encoding)
  end

  def test_write_binary
    str = "\xff\xff\x00\x00\xff"
    expect = str.freeze
    assert_equal Encoding::BINARY, str.encoding if str.respond_to?(:encoding)
    @io.write_binary(str)
    wr = @raw.string
    assert_equal "\x80\x05#{expect}", wr
    assert_equal Encoding::BINARY, wr.encoding if wr.respond_to?(:encoding)
  end

  def test_read_write_binary_big
    @io.keep_binary = true
    [ 127, 128, 129, 5, 200 ].each do |i|
      str = "\x0f" * i
      expect = str.freeze
      assert_equal Encoding::BINARY, str.encoding if str.respond_to?(:encoding)
      @raw.seek(0)
      @raw.truncate(0)
      @io.write_binary(str)
      @raw.seek(0)
      rd = @io.gets
      assert_equal Encoding::BINARY, rd.encoding if str.respond_to?(:encoding)
      assert_equal rd, expect
    end
  end

  def test_write
    str = "\xff\xff\x00\x00\xff"
    expect = str.freeze
    assert_equal Encoding::BINARY, str.encoding if str.respond_to?(:encoding)
    @io.write(str)
    wr = @raw.string
    assert_equal "\x80\x05#{expect}", wr
    assert_equal Encoding::BINARY, wr.encoding if wr.respond_to?(:encoding)

    @raw.truncate(0)
    @raw.seek(0)
    assert_equal "", @raw.string

    str = "hello world"
    if str.respond_to?(:encode)
      str.force_encoding(Encoding::UTF_8)
      assert_equal Encoding::UTF_8, str.encoding
    end
    @io.write(str)
    wr = @raw.string
    assert_equal "\0#{str}\xff", wr
    assert_equal Encoding::BINARY, wr.encoding if wr.respond_to?(:encoding)
  end

  def test_utf8_limit
    max = Sunshowers::IO::MAX_UTF8_SIZE
    @raw.string << "\0#{'.' * max}\xff"
    assert_equal max, @io.gets.length
  end

  def test_utf8_over_limit
    max = Sunshowers::IO::MAX_UTF8_SIZE + Sunshowers::IO::RD_SIZE
    @raw.string << "\0#{'.' * max}\xff"
    assert_raises(Sunshowers::ProtocolError) { @io.gets }
  end

  def test_binary_limit
    @io.keep_binary = true
    max = Sunshowers::IO::MAX_BINARY_SIZE
    @io.write_binary('.' * max)
    @raw.seek(0)
    assert_equal('.' * max, @io.gets)
  end

  def test_binary_over_limit
    @io.keep_binary = true
    max = Sunshowers::IO::MAX_BINARY_SIZE + Sunshowers::IO::RD_SIZE
    @io.write_binary('.' * max)
    @raw.seek(0)
    assert_raises(Sunshowers::ProtocolError) { @io.gets }
  end

end
