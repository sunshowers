#!/bin/sh
. ./test-lib.sh
t_plan 3 "basic tests"

t_begin "setup and start" && {
        rack_server_start
	rtmpfiles ec_out ec_err
}

t_begin "send request" && {
	server=$(expr $listen : '\(.*\):.*')
	port=$(expr $listen : '.*:\(.*\)')
	python echo_client.py -s $server -p $port > $ec_out 2> $ec_err
}

t_begin "check errors" && {
	dbgcat r_err
	dbgcat ec_out
	dbgcat ec_err
	check_stderr
}

teardown
t_done
