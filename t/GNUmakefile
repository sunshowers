# we can run tests in parallel with GNU make

all::

pid := $(shell echo $$PPID)

RUBY = ruby
my_lib := $(shell cd ../lib && pwd)
-include ../local.mk
ifeq ($(RUBY_VERSION),)
  RUBY_VERSION := $(shell $(RUBY) -e 'puts RUBY_VERSION')
endif

ifeq ($(RUBY_VERSION),)
  $(error unable to detect RUBY_VERSION)
endif

ifeq ($(RUBYLIB),)
  RUBYLIB := $(my_lib)
else
  RUBYLIB := $(my_lib):$(RUBYLIB)
endif
export RUBYLIB RUBY_VERSION

T = $(wildcard t[0-9][0-9][0-9][0-9]-*.sh)

all:: $(T)

# can't rely on "set -o pipefail" since we don't require bash or ksh93 :<
t_pfx = trash/$@-$(RUBY_VERSION)
TEST_OPTS =
# TRACER = strace -f -o $(t_pfx).strace -s 100000
# TRACER = /usr/bin/time -o $(t_pfx).time

ifdef V
  ifeq ($(V),2)
    TEST_OPTS += --trace
  else
    TEST_OPTS += --verbose
  endif
endif

dependencies := python
deps := $(addprefix .dep+,$(dependencies))
$(deps): dep_bin = $(lastword $(subst +, ,$@))
$(deps):
	@which $(dep_bin) > $@.$(pid) 2>/dev/null || :
	@test -s $@.$(pid) || \
	  { echo >&2 "E '$(dep_bin)' not found in PATH=$(PATH)"; exit 1; }
	@mv $@.$(pid) $@
dep: $(deps) echo_client.py

echo_client.py:
	svn cat -r139 \
	  http://pywebsocket.googlecode.com/svn/trunk/src/example/$@ \
	  > $@.$(pid)
	mv $@.$(pid) $@

trash/.exists:
	test -d trash || mkdir trash
	> $@

$(T): dep trash/.exists
	$(TRACER) $(SHELL) $(SH_TEST_OPTS) $@ $(TEST_OPTS)

clean:
	$(RM) -r trash/*.log trash/*.code

.PHONY: $(T) clean
