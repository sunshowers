# -*- encoding: binary -*-

module Sunshowers

  # Wraps IO objects and provides transparent framing of #gets and
  # #write methods.
  #
  # It is compatible with core Ruby IO objects, Revactor and
  # Rainbows::Fiber::IO objects.
  #
  # The +keep_binary+ accessor method is considered a temporary member
  # of this Struct-based class and likely to change.  Note, we need to
  # be careful about adding extra Struct members since the first three
  # Struct members are "free" in Ruby 1.9
  class IO < Struct.new(:to_io, :buf, :keep_binary)

    # most Web Socket messages from clients are expected to be small text,
    # so we only read 128 bytes off the socket at a time
    RD_SIZE = 128

    # maximum size of a UTF-8 buffer we'll allow in memory (16K)
    # this is a soft limit and may be offset by the value of RD_SIZE
    MAX_UTF8_SIZE = 1024 * 16

    # maximum size of a binary buffer we'll allow in memory (112K)
    # this is a soft limit and may be offset by the value of RD_SIZE
    MAX_BINARY_SIZE = 1024 * 112

    # Web Sockets usually uses UTF-8 when interfacing with the client
    # Ruby Sockets always return strings of Encoding::Binary under 1.9
    ENC = defined?(Encoding::UTF_8) ? Encoding::UTF_8 : nil

    Z = "" # :nodoc:

    # Wraps the given +io+ with Web Sockets-compatible framing.
    #
    #   TCPSocket.new('example.com', 80)
    #   io = Sunshowers::IO.new(socket)
    def initialize(io, buffer = Z.dup)
      super
    end

    # iterates through each message until a client closes the connection
    # or block the issues a break/return
    def each(&block)
      while str = gets
        yield str
      end
      self
    end

    # Retrieves the next record, returns nil if client closes the connection.
    # The record may be either a UTF-8 or binary String, under
    # Ruby 1.9, the String#encoding will be set appropriately.  Binary
    # frames will only be returned if #keep_binary is true, otherwise
    # they will be read off the socket and discarded.
    def gets
      begin
        unless buf.empty?
          buf.gsub!(/\A\x00(.*?)\xff/m, Z) and return utf8!($1)
          rv = read_binary and return rv
          buf.size > MAX_UTF8_SIZE and
            raise ProtocolError, "buffer too large #{buf.size}"
        end
        buf << read
      rescue EOFError
        return
      end while true
    end

    def syswrite(buf) # :nodoc:
      to_io.write(buf)
      rescue EOFError,Errno::ECONNRESET,Errno::EPIPE,
             Errno::EINVAL,Errno::EBADF => e
        raise ClientShutdown, e.message, []
    end

    # writes a UTF-8 frame containing +buf+.  For performance reasons,
    # we do not validate that +buf+ contains valid UTF-8, we assume you
    # know what you are doing if you call this method directly.
    def write_utf8(buf)
      syswrite("\0#{binary(buf)}\xff")
    end

    # writes a binary frame containing +buf+.  Avoid using this as
    # the current Web Sockets protocol specification instructs
    # receivers to discard binary data.
    def write_binary(buf)
      buf = binary(buf)
      n = buf.size
      length = []
      begin
        length.unshift((n % 128) | 0x80)
      end while (n /= 128) > 0
      length[-1] ^= 0x80

      syswrite("\x80#{length.pack("C*")}#{buf}")
    end

    if ENC.nil?
      # :stopdoc:
      U_STAR = "U*"
      def valid_utf8?(buf)
        buf.unpack(U_STAR)
        true
        rescue ArgumentError
          false
      end

      def write(buf)
        valid_utf8?(buf) ? write_utf8(buf) : write_binary(buf)
      end
      # :startdoc:
    else

      # Writes out +buf+ as a Web Socket frame.  If +buf+ encoding is UTF-8,
      # then it will be framed as UTF-8, otherwise it will be framed as binary
      # with an explicit length set.  Keep in mind that the current protocol
      # draft specifies binary frames be discarded, So be careful that
      # +buf.encoding+ is Encoding::UTF_8 under Ruby 1.9 (or just call
      # write_utf8(buf) if you're sure +buf+ is valid UTF-8
      def write(buf)
        buf.encoding == ENC ? write_utf8(buf) : write_binary(buf)
      end
    end

    # :stopdoc:
    def read(size = nil)
      i = to_io
      # read with no args for Revactor compat
      i.respond_to?(:readpartial) ?
        i.readpartial(size.nil? ? RD_SIZE : size) :
        i.read(size.nil? ? nil : size)
    end

    if ENC.nil?
      def utf8!(buf)
        valid_utf8?(buf) or raise ProtocolError, "not UTF-8: #{buf.inspect}"
        buf
      end
      def binary(buf); buf; end
    else
      def utf8!(buf)
        buf.force_encoding(ENC)
        buf.valid_encoding? or raise ProtocolError, "not UTF-8: #{buf.inspect}"
        buf
      end
      def binary(buf)
        buf.encoding == Encoding::BINARY ?
          buf : buf.dup.force_encoding(Encoding::BINARY)
      end
    end

    if Z.respond_to?(:ord)
      def ord(byte_str); byte_str.ord; end
    else
      def ord(byte_str); byte_str; end
    end

    def read_binary
      (ord(buf[0]) & 0x80) == 0x80 or return

      i = 1
      b = length = 0
      begin
        buf << read while (b = buf[i]).nil?
        b = ord(b)
        length = length * 128 + (b & 0x7f)
        i += 1
      end while (b & 0x80) != 0

      length > MAX_BINARY_SIZE and
        raise ProtocolError, "chunk too large: #{length} bytes"

      to_read = length - buf.size + i
      while to_read > 0
        buf << (tmp = read)
        to_read -= tmp.size
      end
      rv = buf[i, length]
      buf.replace(buf[i+length, buf.size])

      # The current protocol draft (rev 66), section 4.2 specifies that
      # binary frames should be discarded.
      keep_binary ? rv : false
    end

    # :startdoc:
  end
end
