# -*- encoding: binary -*-
module Sunshowers

  # This module is intended to be used in subclasses of Rack::Request
  # This is normally used inside Sunshowers::Request, but may be included
  # in Rack::Request-derived subclasses such as Sinatra::Request or
  # ActionController::Request.
  module WebSocket

    # Raised when processing of the current request is halted by the server.
    # Rainbows! will not log errors or backtraces for EOFError-derived
    # exceptions.  You may rescue Sunshowers::WebSocket::Quit in other
    # middlewares for logging (and re-raise it afterwards).
    class Quit < EOFError; end

    # the pywebsocket/src/example/echo_client.py has a hard-coded dependency
    # on the first 3 lines of the following response.
    # Order and case both matter for the first 3 lines
    HANDSHAKE_HEAD =
      "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" \
      "Upgrade: WebSocket\r\n" \
      "Connection: Upgrade\r\n" \
      "WebSocket-Origin: %s\r\n" \
      "WebSocket-Location: %s\r\n"

    # returns true if WebSockets may be used, false otherwise
    def ws?
      # yes, case sensitive header values
      @env["HTTP_CONNECTION"] =~ /\AUpgrade\z/ &&
          @env["HTTP_UPGRADE"] =~ /\AWebSocket\z/ &&
          origin &&
          @env["HTTP_HOST"]
    end

    # returns the contents of the Origin: header
    def origin
      @env["HTTP_ORIGIN"]
    end unless method_defined?(:origin)

    # performs the Web Sockets handshake, only call this once
    # per application dispatch (unless you want to test error
    # handling behavior in your clients).
    def ws_handshake!(headers = {})
      ws? or raise HandshakeError, "not a Web Sockets request"
      tmp = [ sprintf(HANDSHAKE_HEAD, origin, ws_location) ]
      wsp = ws_protocol and tmp << "WebSocket-Protocol: #{wsp}\r\n"
      headers.each do |key, value|
        if value =~ /\n/
          tmp.concat(value.split(/\n/).map! { |v| "#{key}: #{v}\r\n" })
        else
          tmp << "#{key}: #{value}\r\n"
        end
      end
      tmp << "\r\n"

      # get the bare IO-ish object that does not encode, this can be
      # a Rainbows::Fiber::IO object
      ws_io.to_io.write(tmp.join(""))
    end

    # returns the underlying protocol used
    def ws_protocol
      protocol = @env["HTTP_WEBSOCKET_PROTOCOL"] or return
      protocol =~ /\A[\x20-\x7e]+\z/ or
        raise HandshakeError, "Invalid WebSocket-Protocol: #{protocol}"
      protocol
    end

    # returns the requested resource name
    def ws_resource_name
      @env["REQUEST_URI"] || fullpath
    end

    # returns the ws://... or wss://... URL
    def ws_location
      s = ws_scheme
      rv = "#{s}://#{host}"
      if (s == "wss" && port != 443) || (s == "ws" && port != 80)
        rv << ":#{port}"
      end
      rv << ws_resource_name #concat the resource name
      rv
    end

    # returns a Sunshowers::IO object
    def ws_io
      @ws_io ||= IO.new(@env["hack.io"])
    end

    # returns "ws" (unencrypted) or "wss" (SSL)
    def ws_scheme
      scheme == "https" ? "wss" : "ws"
    end

    # Terminates the Web Sockets request/response cycle and
    # current Rack application dispatch.  Other middlewares
    # may rescue Sunshowers::WebSocket::Quit to avoid this.
    # This does NOT attempt to close the socket, that is still
    # the responsibility of the web server.
    def ws_quit!
      raise Quit, "done processing WebSocket request"
    end

  end
end
