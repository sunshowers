# -*- encoding: binary -*-
require 'rack'

module Sunshowers

  # This is both a request object and a response helper, as Web Sockets
  # is typically used bidirectionally simutaneously.  It may be used in
  # place of Rack::Request for applications that use Rack::Request
  # directly.  This is just a subclass of Rack::Request that adds all
  # methods from the Sunshowers::WebSocket module.
  class Request < Rack::Request
    include WebSocket
  end

end
