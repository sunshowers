# -*- encoding: binary -*-

module Sunshowers

  # version of Sunshowers, MAJOR.MINOR.TEENY
  VERSION = "0.2.0"

  # base class for most Sunshowers errors (except ClientShutdown)
  class Error < StandardError; end

  # raised when errors are encountered during handshaking
  # (protocol validation failure)
  class HandshakeError < Error; end

  # raised when we have received bad (or overly large) messages
  # or invalid UTF-8 for UTF-8 frames
  class ProtocolError < Error; end

  # this is raised when a client socket becomes unwritable
  class ClientShutdown < EOFError; end

  autoload :IO, 'sunshowers/io'
  autoload :Request, 'sunshowers/request'
  autoload :WebSocket, 'sunshowers/web_socket'
end
